CREATE DATABASE blog
    DEFAULT CHARACTER SET utf8;
USE blog;

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE news (
    id int(11) NOT NULL AUTO_INCREMENT,
    title varchar(100) NOT NULL UNIQUE,
    url varchar(255),
    biografia text NOT NULL,
    cover varchar(100) NULL,
PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;